package uml_diagram;

public class RentedVehicle {
	
	private double basefee;

	public RentedVehicle(double basefees) {
		super();
		this.basefee = basefees;
	}

	public double getBasefee() {
		return basefee;
	}

	public void setBasefee(double basefee) {
		this.basefee = basefee;
	}

}
