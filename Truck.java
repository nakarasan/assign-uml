package uml_diagram;

public class Truck extends FuelVehicle {
	
	private int capacity;

	public Truck(double basefee, double nbKms) {
		super(basefee, nbKms);
	}

	@Override
	public double getMileageFees() {
		return super.getMileageFees();
	}

	public double getCost() {
		return (capacity * getBasefee()) + getMileageFees();
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getCapacity() {
		return capacity;
	}


}
