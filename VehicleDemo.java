package uml_diagram;

import java.util.Scanner;
public class VehicleDemo {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Welcome to Vechile Rental Company");

		String[] rentedVechile = { "1.Car", "2.Truck", "3.Bicycle" };

		for (int i = 0; i < rentedVechile.length; i++) {
			System.out.println(rentedVechile[i]);
		}
		System.out.println("Please enter the the number");
		int choice = input.nextInt();
		switch (choice) {
		case 1:
			Car car = new Car(300, 180, choice);
			car.setNbSeats(5);
			System.out.println("Alto car is Selected");
			System.out.println("Number of seats: " + car.getNbSeats());
			System.out.println("Cost of rent: " + "Rs." + car.getCost());
			break;
		case 2:
			Truck truck = new Truck(500, 30);
			System.out.println("Monster Truck is Selected");
			truck.setCapacity(6500);
			System.out.println("Truck Capacity: " + truck.getCapacity());
			System.out.println("Cost of rent: " + "Rs." + truck.getCost());
			break;
		case 3:
			Bicycle bicycle = new Bicycle(100, 30);
			System.out.println("Rally bicycle is Selected");
			System.out.println("Number of days for rent: " + bicycle.getNbDays());
			System.out.println("Cost of rent: " + "Rs." + bicycle.getCost());
			break;
		}

	}

}
