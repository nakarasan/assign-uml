package uml_diagram;

public class Bicycle extends RentedVehicle {

	private int nbDays;

	public Bicycle(double basefee, double i) {
		super(basefee);
	}

	public Bicycle(double basefee, int nbDays) {
		super(basefee);
		this.nbDays = nbDays;
	}

	public int getNbDays() {
		return nbDays;
	}

	public void setNbDays() {
		this.nbDays = nbDays;
	}

	public double getCost() {
		return nbDays * getBasefee();
	}
}
